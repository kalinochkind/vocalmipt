import datetime

from django.contrib.auth import login
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.views.generic import TemplateView, FormView

from . import forms
from .models import Page, Profile, ScheduleRecord, Parameter, Code


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        path = kwargs.pop('path', '')
        page = Page.objects.filter(path=path).first()
        if page:
            context['html'] = page.html
        else:
            context['not_found'] = True
        context['path'] = path
        return context


class SignupView(FormView):
    template_name = 'signup.html'
    form_class = forms.SignupForm

    @transaction.atomic
    def form_valid(self, form):
        user = User.objects.create_user(username=form.cleaned_data['username'], password=form.cleaned_data['password'],
                                        email=form.cleaned_data['email'])
        Profile.objects.create(user=user, balance=int(Parameter.get('default_money')))
        login(self.request, user)
        return HttpResponseRedirect('/schedule')


WEEKDAYS = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье']


class ScheduleView(TemplateView):
    template_name = 'schedule.html'

    def get_context_data(self):
        context = super().get_context_data()

        now = timezone.now().astimezone(timezone.get_default_timezone())
        days = []
        for i in range(7):
            date = now + datetime.timedelta(days=i)
            days.append((WEEKDAYS[date.weekday()], date))
        start_hour = int(Parameter.get('start_hour'))
        end_hour = int(Parameter.get('end_hour'))
        start_time = now.replace(hour=start_hour, minute=0, second=0, microsecond=0)
        schedule = []
        records = ScheduleRecord.objects.filter(time__gte=start_time).select_related('user')
        records = {int(r.time.timestamp()): r for r in records}
        for i in range((end_hour - start_hour) * 4):
            next_time = start_time + datetime.timedelta(minutes=15 * i)
            next_time_end = start_time + datetime.timedelta(minutes=15 * (i + 1))
            cells = []
            for j in range(7):
                code = None
                cell_time = int((next_time + datetime.timedelta(days=j)).timestamp())
                if cell_time in records:
                    owner = records[cell_time].user.username
                    code = records[cell_time].code
                elif j == 0 and next_time_end <= now:
                    owner = ''
                else:
                    owner = None
                cells.append({'ts': cell_time, 'owner': owner, 'code': code})
            schedule.append({'start': next_time, 'end': next_time_end, 'cells': cells})
        context['days'] = days
        context['schedule'] = schedule
        context['cell_price'] = int(Parameter.get('price'))
        context['user_money'] = self.request.user.profile.balance if self.request.user.is_authenticated else 0
        context['path'] = 'schedule'
        return context

    @transaction.atomic
    def post(self, request):
        times = list(map(int, request.POST.get('cells', '').split(',')))
        Profile.objects.filter(user=request.user).select_for_update()
        price = int(Parameter.get('price'))
        if not request.user.is_authenticated or len(times) * price > request.user.profile.balance:
            return HttpResponseRedirect('/schedule')
        now = timezone.now().astimezone(timezone.get_default_timezone())
        start_time = now.replace(hour=0, minute=0, second=0, microsecond=0)
        end_time = start_time + datetime.timedelta(days=7)
        dates = []
        start_hour = int(Parameter.get('start_hour'))
        end_hour = int(Parameter.get('end_hour'))
        for ts in times:
            date = datetime.datetime.fromtimestamp(ts, timezone.get_default_timezone()).replace(second=0, microsecond=0)
            if date < start_time or date >= end_time or date.hour < start_hour or date.hour >= end_hour or date.minute % 15 != 0:
                return HttpResponseRedirect('/schedule')
            dates.append(date)
        request.user.profile.balance -= price * len(dates)
        request.user.profile.save()
        for date in dates:
            ScheduleRecord.objects.create(user=request.user, time=date, code=Code.random())
        return HttpResponseRedirect('/schedule')
