import random
from django.contrib.auth.models import User
from django.db import models


class Page(models.Model):

    path = models.CharField('Адрес', max_length=50, blank=True)
    html = models.TextField('HTML')

    def __str__(self):
        return self.path or '<главная>'

    class Meta:
        verbose_name = 'Страница'
        verbose_name_plural = 'Страницы'


class Parameter(models.Model):

    key = models.CharField(max_length=100, unique=True)
    name = models.CharField('Параметр', max_length=100)
    value = models.CharField('Значение', max_length=100)

    def __str__(self):
        return self.name

    @staticmethod
    def get(key):
        return Parameter.objects.get(key=key).value

    class Meta:
        verbose_name = 'Параметр'
        verbose_name_plural = 'Параметры'


class Profile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    balance = models.IntegerField('Баланс', default=0)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'


class Code(models.Model):

    code = models.CharField('Код', max_length=10)

    def __str__(self):
        return self.code

    @classmethod
    def random(cls):
        codes = list(cls.objects.all())
        if not codes:
            return None
        return random.choice(codes)

    class Meta:
        verbose_name = 'Код'
        verbose_name_plural = 'Коды'


class ScheduleRecord(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    time = models.DateTimeField('Время', unique=True)
    code = models.ForeignKey(Code, on_delete=models.SET_NULL, blank=True, null=True, verbose_name='Код')

    def __str__(self):
        return f'{self.user.username}, {self.time}'

    class Meta:
        verbose_name = 'Запись'
        verbose_name_plural = 'Записи'
