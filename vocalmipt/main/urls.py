from django.contrib.auth import views as auth_views
from django.urls import path
from django.views.generic import RedirectView

from . import views

app_name = 'main'


urlpatterns = [
    path('', RedirectView.as_view(permanent=True, url='/schedule')),
    path('logout', auth_views.LogoutView.as_view(next_page='/')),
    path('login', auth_views.LoginView.as_view(template_name='login.html')),
    path('signup', views.SignupView.as_view()),
    path('schedule', views.ScheduleView.as_view()),
    path('<str:path>', views.IndexView.as_view()),
]
