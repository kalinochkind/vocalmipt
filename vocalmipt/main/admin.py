from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from . import models


class PageAdmin(admin.ModelAdmin):
    readonly_fields = ['path']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(models.Page, PageAdmin)


class ProfileInline(admin.StackedInline):
    model = models.Profile


class UserAdmin(UserAdmin):
    inlines = [ProfileInline]


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(models.ScheduleRecord)


class ParameterAdmin(admin.ModelAdmin):
    readonly_fields = ['name']
    exclude = ['key']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(models.Parameter, ParameterAdmin)
admin.site.register(models.Code)
