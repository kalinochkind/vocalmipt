from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


class SignupForm(forms.ModelForm):
    email = forms.EmailField(required=True)
    password_repeat = forms.CharField()

    def clean_password_repeat(self):
        pass1 = self.data['password']
        pass2 = self.data['password_repeat']
        if pass1 != pass2:
            raise ValidationError("Пароли не совпадают")
        return pass2

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
